package com.psyfire.util.node;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class NodeIterator<T>  implements Iterator<NodeMap<T>> {
	private final NodeMap<T> root;
	private final List<NodeMap<T>> branches;
	NodeIterator<T> subIterator;
	int index = -1;

	public NodeIterator(NodeMap<T> root) {
		this.root = root;
		branches = root.children();
	}

	@Override
	public boolean hasNext() {
		if (subIteratorHasNext()) {
			return true;
		}
		return index < branches.size();
	}

	private boolean subIteratorHasNext() {
		return subIterator != null && subIterator.hasNext();
	}

	/* Navigates in order you might expect a fully expanded file system "tree". Folder is returned first, then the contents of the folder.*/
	@Override
	public NodeMap<T> next() {
		if (!hasNext()) throw new NoSuchElementException();
		if (index == -1) {
			index++;
			return root;
		}

		if (subIteratorHasNext()) {
			return subIterator.next();
		}

		subIterator = new NodeIterator<>(branches.get(index));
		index++;
		return subIterator.next();
	}

	public Stream<NodeMap<T>> toStream() {
		Iterable<NodeMap<T>> iterable = () -> this;
		return StreamSupport.stream(iterable.spliterator(), false);
	}
}
