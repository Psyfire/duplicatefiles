package com.psyfire.util.node;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

//TODO rename?
/* *Not yet complete, but this is an experimental functional Json library */
public class NodeMap<T> extends Streamer<NodeMap<T>> {
	private final String name;

	private final T value;

	private NodeMap<T> parent;
	private Map<String, NodeMap<T>> nodes = new HashMap<>();

	public NodeMap(String name) {
		this.name = name;
		this.value = null;
	}

	public NodeMap(String name, T value) {
		this.name = name;
		this.value = value;
	}

	public String name() {
		return name;
	}
	/* =============================== Get or Add Children ======================= */


	public NodeMap<T> add(NodeMap<T> ... nodes) {
		for (NodeMap<T> node : nodes) {
			add(node);
		}
		return this;
	}

	public NodeMap<T> add(NodeMap<T> node) {
		nodes.put(node.name(), node);
		return this;
	}

	/* Creates and adds a new sub-node.  Note taht returned node is 'this' and not the new sub-node. */
	public NodeMap<T> add(String key, T value) {
		NodeMap<T>node = new NodeMap<>(key, value);
		return add(node);
	}


	//TODO value could be null.  Use optional?
	public NodeMap get(String key) {
		return nodes.get(key);
	}

	public NodeMap get(String ... keys) {
		NodeMap<T> current = this;
		for (String key : keys) {
			current = current.get(key);
			if (current == null) {
				return null;
			}
		}
		return current;
	}




	/* ============================== Value Operations ============================== */

	//TODO value could be null.  Use optional?
	public T value() {
		return value;
	}

	/* Like a for-each, this only executes if the value is present.*/
	public void ifValue(Consumer<? super NodeMap<T>> action) {
		if (value() != null) {
			action.accept(this);
		}
	}

	/* ============================== Node Relationships ============================== */

	public List<String> path() {
		List<String> path = (parent() != null) ?
				parent().path() :
				new ArrayList<>();

		path.add(name());
		return path;
	}

	public NodeMap<T> parent() {
		return parent;
	}



	public void setParent(NodeMap<T> parent) {
		this.parent = parent;
	}

	public List<NodeMap<T>> children() {
		List<NodeMap<T>> list = new ArrayList<>();
		list.addAll(nodes.values());
		return list;
	}

	/* The number of imediate children. */
	public int size() {
		return nodes.size();
	}

	/* ============================== Functional ============================ */

	/* Returns a stream of this node, and all of it's children recursively.*/
	public Stream<NodeMap<T>> stream() {
		return iterator().toStream();
	}

	/* Returns an iterator of this node, and all of it's children recursively.*/
	public NodeIterator<T> iterator() {
		return new NodeIterator<>(this);
	}
}
