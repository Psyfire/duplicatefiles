package com.psyfire.jsonNode.jsonFunctional;

public enum JsonType {
	JSON_OBJECT, JSON_ARRAY, JSON_VALUE;
}
