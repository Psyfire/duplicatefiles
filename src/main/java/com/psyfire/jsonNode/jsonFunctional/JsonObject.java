package com.psyfire.jsonNode.jsonFunctional;

import com.psyfire.util.node.NodeMap;

public class JsonObject<T> extends NodeMap<T> {
	public JsonObject(String key) {
		super(key, null);
	}
}
