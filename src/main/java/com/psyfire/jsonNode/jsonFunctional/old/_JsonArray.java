package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class _JsonArray<T> extends _JsonNode<T> {
	List<_JsonNode<T>> nodes = new ArrayList<>();
	private final String key;

	public _JsonArray(String key){
		this.key = key;
	}

	public _JsonArray add(_JsonNode node) {
		nodes.add(node);
		return this;
	}

	public _JsonNode get(int index) {
		return nodes.get(index);
	}

	public int size() {
		return nodes.size();
	}

	@Override
	public _JsonType type() {
		return _JsonType.JSON_ARRAY;
	}

	@Override
	public String key() {
		return key;
	}

	@Override
	public List<_JsonNode<T>> children() {
		List<_JsonNode<T>> list = new ArrayList<>();
		list.addAll(nodes);
		return list;
	}

	@Override
	public void accept(Consumer<_JsonObject<T>> objectConsumer, Consumer<_JsonArray<T>> arrayConsumer, Consumer<_JsonField<T>> valueConsumer) {
		arrayConsumer.accept(this);
	}

	@Override
	public <R> R apply(Function<_JsonObject<T>, R> objectFunction, Function<_JsonArray<T>, R> arrayFunction, Function<_JsonField<T>, R> valueFunction) {
		return arrayFunction.apply(this);
	}


}
