package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/* *Not yet complete, but this is an experimental functional Json library */
public abstract class _JsonNode<T> extends _Streamer<_JsonNode<T>> {// implements Stream<JsonNode<T>>

	/* Returns a stream of this node, and all of it's children recursively.*/
	public abstract _JsonType type();

	public abstract String key();

	/* Returns this node's immediate children. */
	public abstract List<_JsonNode<T>> children();

	public Stream<_JsonNode<T>> stream() {
		return iterator().toStream();
	}

	/* Runs function on current node, then proceeds to apply to children recursively.*/
	public void forAll(Consumer<_JsonObject<T>> objectConsumer, Consumer<_JsonArray<T>> arrayConsumer, Consumer<_JsonField<T>> valueConsumer) {
		accept(objectConsumer, arrayConsumer, valueConsumer);
		stream().forEach(node -> node.forAll(objectConsumer, arrayConsumer, valueConsumer));
	}

	/* Runs the appropriate consumer on this node. */
	public abstract void accept(Consumer<_JsonObject<T>> objectConsumer, Consumer<_JsonArray<T>> arrayConsumer, Consumer<_JsonField<T>> valueConsumer);

	public abstract <R> R apply(Function<_JsonObject<T>, R> objectFunction, Function<_JsonArray<T>, R> arrayFunction, Function<_JsonField<T>, R> valueFunction);

	public abstract int size();

	/* Returns a stream of this node, and all of it's children recursively.*/
	public _NodeIterator<T> iterator() {
		return new _NodeIterator<>(this);
	}
}
