package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.Comparator;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/* Adds most commonly used Stream functionality to extending classes, without needing to call a stream() method. */
public abstract class _Streamer<T> {
	public abstract Stream<T> stream();
	
	public Stream<T> filter(Predicate<? super T> predicate) {
		return stream().filter(predicate);
	}

	public <R> Stream<R> map(Function<? super T, ? extends R> mapper) {
		return stream().map(mapper);
	}

	public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
		return stream().flatMap(mapper);
	}

	public Stream<T> sorted() {
		return stream().sorted();
	}

	public Stream<T> sorted(Comparator<? super T> comparator) {
		return stream().sorted(comparator);
	}
	
	public void forEach(Consumer<? super T> action) {
		stream().forEach(action);
	}
	
	public long count() {
		return stream().count();
	}
	
	public boolean anyMatch(Predicate<? super T> predicate) {
		return stream().anyMatch(predicate);
	}

	
	public boolean allMatch(Predicate<? super T> predicate) {
		return stream().allMatch(predicate);
	}

	
	public boolean noneMatch(Predicate<? super T> predicate) {
		return stream().noneMatch(predicate);
	}
	
	public Iterator<T> iterator() {
		return stream().iterator();
	}
}
