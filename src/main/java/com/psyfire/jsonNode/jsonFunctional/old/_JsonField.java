package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class _JsonField<T> extends _JsonNode<T> {
	private final T value;
	private final String key;

	public _JsonField(String key, T value) {
		this.key = key;
		this.value = value;
	}
	
	public T value() {
		return value;
	}
	public _JsonType type() {
		return _JsonType.JSON_VALUE;
	}
	
	public String key() {
		return key;
	}

	@Override
	public List<_JsonNode<T>> children() {
		return new ArrayList<>();
	}

	public void accept(Consumer<_JsonObject<T>> objectConsumer, Consumer<_JsonArray<T>> arrayConsumer, Consumer<_JsonField<T>> valueConsumer) {
		valueConsumer.accept(this);
	}

	@Override
	public <R> R apply(Function<_JsonObject<T>, R> objectFunction, Function<_JsonArray<T>, R> arrayFunction, Function<_JsonField<T>, R> valueFunction) {
		return valueFunction.apply(this);
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public String toString() {
		return value.toString();
	}


}
