package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class _NodeIterator<T>  implements Iterator<_JsonNode<T>> {
	private final _JsonNode<T> root;
	private final List<_JsonNode<T>> branches;
	_NodeIterator<T> subIterator;
	int index = -1;

	public _NodeIterator(_JsonNode<T> root) {
		this.root = root;
		branches = root.children();
	}

	@Override
	public boolean hasNext() {
		if (subIteratorHasNext()) {
			return true;
		}
		return index < branches.size();
	}

	private boolean subIteratorHasNext() {
		return subIterator != null && subIterator.hasNext();
	}

	/* Navigates in order you might expect a fully expanded file system "tree". Folder is returned first, then the contents of the folder.*/
	@Override
	public _JsonNode<T> next() {
		if (!hasNext()) throw new NoSuchElementException();
		if (index == -1) {
			index++;
			return root;
		}

		if (subIteratorHasNext()) {
			return subIterator.next();
		}

		subIterator = new _NodeIterator<>(branches.get(index));
		index++;
		return subIterator.next();
	}

	public Stream<_JsonNode<T>> toStream() {
		Iterable<_JsonNode<T>> iterable = () -> this;
		return StreamSupport.stream(iterable.spliterator(), false);
	}
}
