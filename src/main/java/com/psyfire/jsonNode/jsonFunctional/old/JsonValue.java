package com.psyfire.jsonNode.jsonFunctional.old;

import com.psyfire.jsonNode.jsonFunctional.JsonType;

public class JsonValue<T> {
	private final JsonType type;

	private JsonValue(T fieldValue, JsonType type) {
		this.type = type;
	}
	public static <T> JsonValue<T> field(T fieldValue) {
		return new JsonValue<>(fieldValue, JsonType.JSON_VALUE);
	}


}
