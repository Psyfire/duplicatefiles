package com.psyfire.jsonNode.jsonFunctional.old;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class _JsonObject<T> extends _JsonNode<T> {
	private Map<String, _JsonNode<T>> nodes = new HashMap<>();
	private final String key;

	public _JsonObject<T> add(_JsonNode<T> value) {
		nodes.put(value.key(), value);
		return this;
	}

//	public JsonObject<T> addField(String name, T value) {
//		return add(new JsonField<>(name, value));
//	}
//
//	public JsonObject<T> addArray(String name) {
//		return add(new JsonArray<>(name));
//	}
//
//	public JsonObject<T> addObject(String name) {
//		return add(new JsonObject<>(name));
//	}

	public _JsonObject(String key) {
		this.key = key;
	}

	public _JsonNode get(String key) {
		return nodes.get(key);
	}

	public _JsonType type() {
		return _JsonType.JSON_OBJECT;
	}

	public String key() {
		return key;
	}

	@Override
	public List<_JsonNode<T>> children() {
		List<_JsonNode<T>> list = new ArrayList<>();
		list.addAll(nodes.values());
		return list;
	}

	public void accept(Consumer<_JsonObject<T>> objectConsumer, Consumer<_JsonArray<T>> arrayConsumer, Consumer<_JsonField<T>> valueConsumer) {
		objectConsumer.accept(this);
	}

	public <R> R apply(Function<_JsonObject<T>, R> objectFunction, Function<_JsonArray<T>, R> arrayFunction, Function<_JsonField<T>, R> valueFunction) {
		return objectFunction.apply(this);
	}
	public int size() {
		return nodes.size();
	}

}
