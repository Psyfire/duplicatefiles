package com.psyfire.jsonNode.jsonFunctional;


import com.psyfire.util.node.NodeMap;

public class JsonField<T> extends NodeMap<T> {
	public JsonField(String key, T value) {
		super(key, value);
	}

	@Override
	public NodeMap<T> add(NodeMap<T> node) {
		throw new IllegalArgumentException("Cannot add children to a JsonField.");
	}
}
