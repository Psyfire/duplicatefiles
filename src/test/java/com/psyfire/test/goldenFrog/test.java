package com.psyfire.test.goldenFrog;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class test {
	@Test
	public void test() {
		String input, expected, result;

		//test all letters
		input = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		expected = "zYxwvUtsrqpOnmlkjIhgfEdcbAzYxwvUtsrqpOnmlkjIhgfEdcbA";
		result = reverseVowelCAse(input);
		assertThat(result, is(expected));

		//test a few non letters
		input = "1234567890>=!.";
		expected = ".!=>0987654321";
		result = reverseVowelCAse(input);
		assertThat(result, is(expected));

		//test arbitrary string.
		input = "This is a test string! 123";
		expected ="321 !gnIrts tsEt A sI sIht";
		result = reverseVowelCAse(input);
		assertThat(result, is(expected));
	}

	/* This method (1) reverses the order oc chars, (2) consonants are lower case, (3) vowels are upper cased.*/
	public static String reverseVowelCAse(String input) {
		char[] chars = input.toCharArray();
		reverseCharsInPlace(chars);
		vowelCase(chars);
		return String.valueOf(chars);
	}


	private static void reverseCharsInPlace(char[] input) {
		for (int i = 0, len = input.length; i < len/2; i++) {
			char swap = input[i];
			input[i] = input[len-1-i];
			input[len-1-i] = swap;
		}
	}

	private static void vowelCase(char[] input) {
		for (int i = 0; i < input.length; i++) {
			input[i] = vowelCase(input[i]);
		}
	}

	private static char vowelCase(char c) {
		if (isLetter(c)) {
			if (isVowel(c)) {
				return Character.toUpperCase(c);
			}
			return Character.toLowerCase(c);
		}
		return c;
	}

	private static boolean isLetter(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
	}

	//y is sometimes considered a vowel.
	private static char[] vowels = {'a','e','i','o','u','y'};
	private static boolean isVowel(char c) {
		char lower = Character.toLowerCase(c);
		for (char vowel : vowels) {
			if (vowel == lower) {
				return true;
			}
		}
		return false;
	}





	
}
