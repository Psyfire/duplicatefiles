package com.psyfire.test.util.node;

import com.psyfire.util.node.NodeMap;
import org.junit.Test;

public class NodeMapTest {

	public static final String ROOT = "root";
	public static final String SUB_1 = "sub1";
	public static final String SUB_2 = "sub2";
	public static final String SUB_3 = "sub3";
	public static final String SUB_1_1 = "sub1_1";
	public static final String SUB_1_2 = "sub1_2";
	public static final String SUB_1_3 = "sub1_3";
	public static final String SUB_2_A = "sub2_A";
	public static final String SUB_2_B = "sub2_B";
	public static final String SUB_2_C = "sub2_C";
	public static final String SUB_3_1 = "sub3_1";

	public static final String SUB_3_VALUE = "hi";
	public static final String SUB_2_A_VALUE = "a";
	public static final String SUB_2_B_VALUE = "b";
	public static final String SUB_2_C_VALUE = "c";
	public static final String SUB_3_1_VALUE = "abc";
	public static final String SUB_1_1_VALUE = "value";


	@Test
	public void nodeBasicsTest() {
		NodeMap<String> root = new NodeMap<>(ROOT),
				sub1 = new NodeMap<>(SUB_1),
				sub2 = new NodeMap<>(SUB_2),
				sub3 = new NodeMap<>(SUB_3, SUB_3_VALUE),
				sub1_1 = new NodeMap<>(SUB_1_1, SUB_1_1_VALUE),
				sub1_2 = new NodeMap<>(SUB_1_2),
				sub1_3 = new NodeMap<>(SUB_1_3),
				sub2_A = new NodeMap<>(SUB_2_A, SUB_2_A_VALUE),
				sub2_B = new NodeMap<>(SUB_2_B, SUB_2_B_VALUE),
				sub2_C = new NodeMap<>(SUB_2_C, SUB_2_C_VALUE),
				sub3_1 = new NodeMap<>(SUB_3_1, SUB_3_1_VALUE);

		root.add(sub1,sub2,sub3);
		sub1.add(sub1_1,sub1_2,sub1_3);
		sub2.add(sub2_A,sub2_B,sub2_C);
		sub3.add(sub3_1);



		System.out.println(root.get(SUB_1, SUB_1_1).path());
		System.out.println(root.get(SUB_1, SUB_1_1).value());

		System.out.println(root.get(SUB_3).path());
		System.out.println(root.get(SUB_3).value());

		root.forEach(node -> {
			System.out.println(node.path() + " : " + node.value());
		});

//		System.out.println("Contains1: "+ root.anyMatch(node -> node.value().equals(SUB_1_1_VALUE)));
//		System.out.println("Contains2: "+ root.anyMatch(node -> node.value().equals(SUB_1_1_VALUE+"fake")));

		root.filter(node -> node.name().startsWith(SUB_3))
			.forEach(node -> System.out.println("Match:" + node.path() + " : " + node.value()));
	}




}
