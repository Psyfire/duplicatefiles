package com.psyfire.test.test1;

import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class DuplicateFileFinder {
	public static Map<Integer, List<File>> findDuplicates(File root) {
		Stream<File> fileStream = new FileSystemIterator(root).toStream();

		Map<Integer, List<File>> fileMap = new HashMap<>();

		//TODO: First pass may be very quick to use file-size as hash, to narrow results..

		/* First pass only does a light weight scan and hash. */
		Function<File, Integer> hashFn = file -> FileHasher.hashFile(file, 2, 16);
		fileStream.filter(file -> file.isFile())
				.forEach(file -> hashAndInsert(file, fileMap, hashFn));

		removeSingleFiles(fileMap);

		/* Refine with a second pass. . */
		Map<Integer, List<File>> fileMap2 = new HashMap<>();

		Function<File, Integer> hashFn2 = file -> FileHasher.hashFile(file, 50, 32);
		fileMap.forEach((key, files) -> {
			files.forEach(file -> hashAndInsert(file, fileMap2, hashFn2));
		});

		removeSingleFiles(fileMap2);

		/* Could refine further, but probably not needed. */

		return fileMap2;
	}

	private static void hashAndInsert(File file, Map<Integer, List<File>> fileMap, Function<File, Integer> hasher) {
		Integer hash = hasher.apply(file);
		if (hash == null) {return;}
		List<File> fileMatches = fileMap.get(hash);

		if (fileMatches == null) {
			fileMatches = new ArrayList<>();
			fileMap.put(hash, fileMatches);
		}

		fileMatches.add(file);
	}

	private static void removeSingleFiles(Map<Integer, List<File>> fileMap) {
		Iterator<Map.Entry<Integer, List<File>>> it;
		Map.Entry<Integer,List<File>> entry;

		for(it =fileMap.entrySet().iterator(); it.hasNext(); ) {
			entry = it.next();
			if (entry.getValue().size() <= 1) {
				it.remove();
			}
		}
	}

}
