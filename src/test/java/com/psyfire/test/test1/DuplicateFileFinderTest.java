package com.psyfire.test.test1;

import org.junit.Test;

import java.io.File;
import java.util.*;
import java.util.stream.Stream;

public class DuplicateFileFinderTest {
	private static final String path ="C:\\Sto-Code\\15_PracticeCode\\src\\test\\resources"; //TODO change this!

	@Test
	public void testIterator() {
		/* note runs iterator, but doesn't really test it.*/
		File root = new File(path);
		if (!root.exists()) {
			throw new IllegalArgumentException("File path does not exist: \""+path+"\"");
		}

		Stream<File> fileStream = new FileSystemIterator(new File(path)).toStream();
		fileStream.forEach(file -> System.out.println(file.getAbsoluteFile()));
	}

	@Test
	public void runDuplicateFinder() {
		/* note runs duplicate finder, but doesn't really test it.*/
		File root = new File(path);
		if (!root.exists()) {
			throw new IllegalArgumentException("File path does not exist: \""+path+"\"");
		}

		Map<Integer, List<File>> duplicates = DuplicateFileFinder.findDuplicates(root);

		String json = DuplicatesJsonSerializer.duplicatesJson(duplicates);
		System.out.println(json);
	}
}
