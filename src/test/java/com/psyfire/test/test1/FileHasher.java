package com.psyfire.test.test1;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class FileHasher {
	/*
	 Number of byes sampled per file will always be sampleSize * blockSize, unless file is smaller than sampleSize * block size.
	 sampleSize: The number of samples taken throughout a file.
	 blockSize: The number of bytes sampled at each sample.
	*/
	public static final Integer hashFile(File file, int sampleSize, int blockSize) {
		try {
			if (file == null) {return null;} /*TODO: null not great design to indicate a failure.*/

			long size = file.length();

			if (size == 0) {return null;} /*TODO: null not great design to indicate a failure.*/
			byte[] byes;

			RandomAccessFile raf = new RandomAccessFile(file, "r");


			/* Handle small file size edge case. */
			if (size < blockSize * sampleSize) {
				byes = new byte[(int) size];
				raf.read(byes, 0, (int) size-1);
				return Arrays.hashCode(byes);
			}

			byes = new byte[sampleSize * blockSize];

			if (blockSize * (sampleSize-blockSize) > size) {
				Long bSize = ((sampleSize - blockSize) / size);

				blockSize = bSize.intValue();
			}

			/* Average spacing of samples through file. */
			Long skipL = (size - blockSize) / sampleSize;

			/* Edge case for very large files. */
			if (skipL >= Integer.MAX_VALUE) {
				skipL = (long) (Integer.MAX_VALUE);
			}

			int skip = skipL.intValue();
			for (int i = 0; i < sampleSize; i++) {
				raf.read(byes, i*blockSize, blockSize);
				raf.seek(skip);
			}

			raf.close();
			return Arrays.hashCode(byes);
		} catch (Exception e) {
			throw new FileHasherException("Unable to hash file.",e);
		}
	}

	public static final Integer hashFileSize(File file) {
		if (file == null || file.length() == 0) {return -1;}

		Long size = file.length();
		return size.hashCode();
	}

	//TODO not great, but temporarily allows me to use this as a lambda without more boiler-plate code.
	public static class FileHasherException extends RuntimeException {
		public FileHasherException(String message, Exception e) {
			super(message, e);
		}
	}
}


